# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=debianutils
pkgver=4.11
pkgrel=0
pkgdesc="Miscellaneous utilities from Debian"
url="https://packages.qa.debian.org/d/debianutils.html"
arch="all"
options="!check"  # No test suite.
license="BSD-3-Clause AND GPL-2.0+"
depends="coreutils mawk"  # awk, cat, and rm are required by add-shell
makedepends="grep"  # early package, declare these
subpackages="$pkgname-doc $pkgname-which::noarch"
source="http://ftp.debian.org/debian/pool/main/d/$pkgname/${pkgname}_${pkgver}.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

which() {
	provider_priority=10
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/which "$subpkgdir"/usr/bin/
	# This will emit a warning about docs on abuild, but this is expected.
	mkdir -p "$subpkgdir"/usr/share/man/man1
	mv "${pkgdir}-doc"/usr/share/man/man1/which* "$subpkgdir"/usr/share/man/man1/
}

sha512sums="87f6b7d9fecbaff615a5529328d384d8adf4ed69c8cc00acc54681d91fc146f4241a761dde2c9b092cb279b63d30dcaa6c2d0309ef4d29d6a2b09600c53c0ac6  debianutils_4.11.tar.xz"
