# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=unzip
pkgver=6.0
_pkgver=$(printf '%s' "$pkgver" | tr -d .)
_debver=25
pkgrel=5
pkgdesc="Extract PKZIP-compatible .zip files"
url="http://www.info-zip.org/UnZip.html"
arch="all"
license="Info-ZIP"
subpackages="$pkgname-doc"
# normally     ftp://ftp.info-zip.org/pub/infozip/src/$pkgname$_pkgver.zip
source="$pkgname-$pkgver.tgz::https://distfiles.adelielinux.org/source/$pkgname$_pkgver.tgz
	http://deb.debian.org/debian/pool/main/u/unzip/unzip_$pkgver-$_debver.debian.tar.xz
	cflags.patch
	format-secure.patch
	unzipsfx-bomb-32bit.patch
	"
builddir="$srcdir/$pkgname$_pkgver"

# secfixes:
#   6.0-r4:
#     - CVE-2014-8139
#     - CVE-2014-8140
#     - CVE-2014-8141
#     - CVE-2014-9636
#     - CVE-2014-9913
#     - CVE-2016-9844
#     - CVE-2018-18384
#     - CVE-2018-1000035
#     - CVE-2019-13232

prepare() {
	cd "$builddir"
	while read -r i; do
		msg "$i"
		patch -p1 -i "../debian/patches/$i"
	done < ../debian/patches/series
	default_prepare
}

build() {
	make -f unix/Makefile \
		CC="${CHOST}-gcc" \
		LOCAL_UNZIP="${CFLAGS} ${CPPFLAGS}" \
		STRIP=: \
		prefix=/usr generic
}

check() {
	make -f unix/Makefile check
}

package() {
	make -f unix/Makefile \
		MANDIR=${pkgdir}/usr/share/man/man1/ \
		prefix=${pkgdir}/usr install
	install -Dm644 LICENSE \
		"$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="0694e403ebc57b37218e00ec1a406cae5cc9c5b52b6798e0d4590840b6cdbf9ddc0d9471f67af783e960f8fa2e620394d51384257dca23d06bcd90224a80ce5d  unzip-6.0.tgz
13c16db420fa4a34be3090a9acdd79b01320da40ac5aa89a9dfca03e64b914b28eb72aff3882d02a8197457bcb8eeb9473c998cf6920e511883c9289a949fb21  unzip_6.0-25.debian.tar.xz
029447a48972234e60c6b45c58b01dbe411594b1ffe0db00d028810b0bcfa7244dcc89f765e1ee6e8805ba2d2db9bc1d05a1e30ef0d9dd08d33ff6f04af811ab  cflags.patch
4bdf55937a181d496261a8f426a97d63844ba96f23beea7906c5e4f7064f55c188ee5ec3ae2d6f2011b5f26b6ac0941dcffb83c06370ed9648b2262917cde64d  format-secure.patch
81777dfa1ad707046b238fa9205f8be0f48363f0f23bc0d2d83b67b143ceeba6818cc11058355195a03432cdd6ed4f392202ff3029e14d4b1435c9e2cb5ca6bf  unzipsfx-bomb-32bit.patch"
