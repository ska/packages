# Maintainer: 
pkgname=libvpx
pkgver=1.7.0
pkgrel=0
pkgdesc="Library for the VP8 and VP9 codecs"
url="https://www.webmproject.org/"
arch="all"
options="!check"  # Requires at least 2 GB of sample videos...
license="BSD-3-Clause"
depends=""
makedepends="bash coreutils perl yasm cmd:which"
subpackages="$pkgname-dev $pkgname-utils"
source="https://github.com/webmproject/$pkgname/archive/v$pkgver/$pkgname-$pkgver.tar.gz"

build() {
	cd "$builddir"
	# fix build for armv7
	case "$CTARGET" in
	armv7-*-*-*eabihf) export CFLAGS="-mfloat-abi=hard";;
	esac
	bash ./configure \
		--extra-cflags="$CFLAGS" \
		--enable-pic \
		--enable-libs \
		--enable-runtime-cpu-detect \
		--enable-vp8 \
		--enable-vp9 \
		--enable-shared \
		--disable-install-srcs \
		--enable-postproc
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DIST_DIR="$pkgdir"/usr install
	chmod 644 "$pkgdir"/usr/include/vpx/*.h \
		"$pkgdir"/usr/lib/pkgconfig/*
	chown root:root -R "$pkgdir"
	chmod 755 "$pkgdir"/usr/lib/*
}

utils() {
	pkgdesc="VP8 and VP9 vode codec utilities and tools"
	install -d "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="8b3b766b550f8d86907628d7ed88035f9a2612aac21542e0fd5ad35b905eb82cbe1be02a1a24afce7a3bcc4766f62611971f72724761996b392136c40a1e7ff0  libvpx-1.7.0.tar.gz"
