# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=labplot
pkgver=2.8.0
pkgrel=0
pkgdesc="Interactive tool for graphing and analysis of scientific data"
url="https://www.kde.org/applications/education/labplot2"
arch="all"
options="!check"  # all tests require X11
license="GPL-2.0-only"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	karchive-dev kcompletion-dev kconfigwidgets-dev kcoreaddons-dev kio-dev
	kdoctools-dev ki18n-dev kiconthemes-dev kdelibs4support-dev kxmlgui-dev
	knewstuff-dev ktextwidgets-dev kwidgetsaddons-dev gsl-dev fftw-dev
	qt5-qtserialport-dev syntax-highlighting-dev bison libexecinfo-dev
	cantor-dev docbook-xsl lz4-dev poppler-dev poppler-qt5-dev
	kuserfeedback-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/labplot/$pkgver/labplot-$pkgver.tar.xz
	cantor.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Debug \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS -D_GNU_SOURCE" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# gives incorrect results
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E fittest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b96a758813a411801ca65082aab54857b2da77a705ea939f2ae49d6d75a3c472b666e61c501283482c7736975b1f589bf0710028a36b237ae2df9e56ac04f0e5  labplot-2.8.0.tar.xz
1d8b81a2eb9e4aa2909881f0ea481252200f5821250d8e8a47ac941bd223bb86f5b1c404c2b374af69b0d491c50f9179a34f30ba5829a5279119761dcf5fe49d  cantor.patch"
