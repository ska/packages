# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=pinentry
pkgver=1.1.0
pkgrel=1
pkgdesc="Collection of passphrase entry handlers for the Assuan protocol"
url="https://www.gnupg.org/related_software/pinentry/"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="gtk+2.0-dev libassuan-dev libcap-dev libgpg-error-dev ncurses-dev
	qt5-qtbase-dev"
subpackages="$pkgname-doc $pkgname-gtk $pkgname-qt5"
source="ftp://ftp.gnupg.org/gcrypt/$pkgname/$pkgname-$pkgver.tar.bz2
	pinentry"

build () {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-pinentry-gtk \
		--enable-pinentry-gtk2 \
		--enable-pinentry-qt \
		--enable-pinentry-curses \
		--enable-fallback-curses
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	rm -f "$pkgdir"/usr/bin/pinentry
	install -D -m755 "$srcdir"/pinentry "$pkgdir"/usr/bin/pinentry
}

gtk() {
	pkgdesc="GTK+ passphrase entry handler"
	install_if="$pkgname=$pkgver-r$pkgrel gtk+2.0"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/pinentry-gtk* \
		"$subpkgdir"/usr/bin/
}

qt5() {
	pkgdesc="Qt 5 passphrase entry handler"
	install_if="$pkgname=$pkgver-r$pkgrel qt5-qtbase"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/pinentry-qt* \
		"$subpkgdir"/usr/bin/
}

sha512sums="5012672925bcb5f683358c259e55e4b87c67cf063ad52c759308933733025c33f7ce08e5b8019ffc101cbf7ef30499040ef2fd34a7611698e65e1593f80948cd  pinentry-1.1.0.tar.bz2
3a6fbbe2ccdbf8d6ed5fddd960fabe9276b04d8ca1a14dded8a5f0de4b8de1636dee2cd343f2db8ff78a66b339faa6d517316f296ca924b8b2add9666f80c433  pinentry"
