# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kguiaddons
pkgver=5.74.0
pkgrel=0
pkgdesc="Framework for creating high-level user interfaces"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires running X11
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtx11extras-dev libxcb-dev libx11-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kguiaddons-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="9943b908fefc12a82678102f439d79ccf4207dd8ebd00ac9cd23d0428cbdc5fcc41d11c04deaf7d63a76a1be24b28c1998e80098b620d7a9c7476ae412640686  kguiaddons-5.74.0.tar.xz"
