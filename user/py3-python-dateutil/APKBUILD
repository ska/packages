# Contributor: Brandon Bergren <git@bdragon.rtk0.net>
# Maintainer: Brandon Bergren <git@bdragon.rtk0.net>
pkgname=py3-python-dateutil
_pkgname=python-dateutil
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=2.8.1
pkgrel=0
pkgdesc="Extensions to the standard Python datetime module"
url="https://pypi.python.org/pypi/python-dateutil"
arch="noarch"
license="Apache-2.0 AND BSD-3-Clause"
depends="python3"
makedepends="python3-dev"
checkdepends="py3-pytest py3-hypothesis py3-freezegun"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/$_p/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# Required with PyTest >=4.8, see:
	# https://bugzilla.redhat.com/show_bug.cgi?id=1734775
	# https://github.com/pytest-dev/pytest/issues/5678
	pytest -W ignore::pytest.PytestUnknownMarkWarning
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

}

sha512sums="337000216e0f8ce32d6363768444144183ab9268f69082f20858f2b3322b1c449e53b2f2b5dcb3645be22294659ce7838f74ace2fd7a7c4f2adc6cf806a9fa2c  py3-python-dateutil-2.8.1.tar.gz"
