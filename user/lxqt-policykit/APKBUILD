# Contributor: Sheila Aman <sheila@vulpine.house>
# Maintainer: Sheila Aman <sheila@vulpine.house>
pkgname=lxqt-policykit
pkgver=0.17.0
_lxqt=0.9.0
pkgrel=0
pkgdesc="PolicyKit agent for LXQT"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qttools-dev polkit-qt-1-dev
	liblxqt-dev>=${pkgver%.*}.0 lxqt-build-tools>=$_lxqt qt5-qttools-dev
	kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/lxqt-policykit/releases/download/$pkgver/lxqt-policykit-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="1cfd4b2d21840704e4b15ae71098e9c6f3d51031293f1fdf6e134ffee867fe4e845caaf24fbd2c107317372b90b6d415a76854977fa8f6b703e708bd09120328  lxqt-policykit-0.17.0.tar.xz"
