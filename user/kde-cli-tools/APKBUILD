# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kde-cli-tools
pkgver=5.18.5
pkgrel=0
pkgdesc="KDE command-like utilities"
url="https://www.kde.org/"
arch="all"
options="!check"  # MIME types for some reason think .doc == .txt
license="(GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0+ AND GPL-2.0-only AND LGPL-2.1-only"
depends=""
checkdepends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev
	qt5-qtx11extras-dev kactivities-dev kcmutils-dev kconfig-dev
	kdeclarative-dev kdesu-dev kdoctools-dev ki18n-dev kiconthemes-dev
	kinit-dev kio-dev kservice-dev kwindowsystem-dev libkworkspace-dev"
subpackages="$pkgname-lang $pkgname-doc"
source="https://download.kde.org/stable/plasma/$pkgver/kde-cli-tools-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c73695a1ecb81b9222e213f0e98c37354560e32dc56fe8e5ebe1665f7f86a88326bd7a32f4b66d2a73826c9d684f5410306ef5e6660751601c77eae0c6f16d4c  kde-cli-tools-5.18.5.tar.xz"
