# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=ruby-diff-lcs
_gemname=${pkgname#ruby-}
pkgver=1.4.4
pkgrel=0
pkgdesc="Generate difference sets between Ruby sequences"
url="http://halostatue.github.io/diff-lcs/"
arch="noarch"
options="!check"  # to avoid cyclic dependency with rspec
license="MIT"
depends="ruby"
subpackages="$pkgname-tools"
source="$pkgname-$pkgver.tar.gz::https://github.com/halostatue/$_gemname/archive/v$pkgver.tar.gz"
builddir="$srcdir/$_gemname-$pkgver"

build() {
	gem build $_gemname.gemspec
}

package() {
	gemdir="$pkgdir/$(ruby -e 'puts Gem.default_dir')"
	geminstdir="$gemdir/gems/$_gemname-$pkgver"

	gem install --local \
		--install-dir "$gemdir" \
		--bindir "$pkgdir/usr/bin" \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname

	# Remove unnecessary files.
	cd "$gemdir"
	rm -r cache/ build_info/ doc/
	cd "$geminstdir"
	rm -r autotest/ docs/ spec/ Rakefile *.md *.rdoc *.txt
}

tools() {
	pkgdesc="$pkgdesc (CLI tools)"
	depends="$pkgname=$pkgver-r$pkgrel"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/* "$subpkgdir"/usr/bin/
}

sha512sums="52fc0501516299396f570be7f35d856ddeed9637430019ca831759467474cecff72be0dace2fed020dacb5f551febef047e3f1928affa0ac0f18b636ee94baa7  ruby-diff-lcs-1.4.4.tar.gz"
