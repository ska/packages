# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gstreamer
pkgver=1.16.2
pkgrel=0
pkgdesc="GStreamer multimedia framework"
url="https://gstreamer.freedesktop.org/"
arch="all"
options="!check"
license="LGPL-2.0+"
depends=""
depends_dev="libxml2-dev"
makedepends="$depends_dev bison flex gobject-introspection-dev glib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools $pkgname-lang"
source="https://gstreamer.freedesktop.org/src/gstreamer/gstreamer-$pkgver.tar.xz
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--enable-introspection \
		--with-package-name="GStreamer (${DISTRO_NAME:-Adélie Linux})" \
		--with-package-origin="${DISTRO_URL:-https://www.adelielinux.org/}" \
		--disable-fatal-warnings \
		--with-ptp-helper-permissions=none
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="Tools and files for GStreamer streaming media framework"
	# gst-feedback needs this
	depends="pkgconfig"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

doc() {
	default_doc
	replaces="${pkgname}1-doc"
}
sha512sums="a8dcdb452a88f411676e54016fcf83149068b2f0b60422bebdbc81220078c61c415512006010ba0299df81ffb59853695c3ce00580f8fc46a079f6aaaa42609f  gstreamer-1.16.2.tar.xz"
