# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=networkmanager-qt
pkgver=5.74.0
pkgrel=0
pkgdesc="Qt framework for NetworkManager"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires the system to have NM running.
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
depends_dev="networkmanager-dev"
makedepends="$depends_dev cmake doxygen extra-cmake-modules qt5-qtbase-dev
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/networkmanager-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="62953a2738cf15633733cd0ae9930c640abe90523495d1481893fc8bd3f2de7ace6f683e541c24f641731e014aff8b2f02520c2644dda4b25fa0460c57110cf3  networkmanager-qt-5.74.0.tar.xz"
