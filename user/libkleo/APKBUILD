# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkleo
pkgver=20.08.1
pkgrel=0
pkgdesc="KDE encryption library"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND GPL-2.0-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev boost-dev gpgme-dev libgpg-error-dev ki18n-dev kwidgetsaddons-dev
	kitemmodels-dev"
makedepends="$depends_dev cmake extra-cmake-modules kcompletion-dev kconfig-dev
	kcodecs-dev kcoreaddons-dev kpimtextedit-dev kwindowsystem-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkleo-$pkgver.tar.xz
	lts.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="9cf56658be7de95382eaf65559158cf793c2c66dd3cbaae784a6793701b575ec71b484172c6ac6f85c579bdc10b1955ba373c77b58be0e8329cec8cb926b6f49  libkleo-20.08.1.tar.xz
ec75b93602cff31a07b72596d8f4fb5519f834f870ca1afeaad7a1dd6a8a0fa1dfb270fe8e3d45ef64001679c89b844e32adc7a0c21e09d46efad751e031f835  lts.patch"
