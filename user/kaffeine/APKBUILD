# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kaffeine
pkgver=2.0.18
pkgrel=0
pkgdesc="Media player with a focus on Digital TV (DVB)"
url="https://www.kde.org/applications/multimedia/kaffeine/"
arch="all"
license="GPL-2.0-only"
depends="vlc"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	kcoreaddons-dev ki18n-dev kwidgetsaddons-dev kwindowsystem-dev kio-dev
	kxmlgui-dev solid-dev kdbusaddons-dev vlc-dev libxscrnsaver-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/kaffeine/kaffeine-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="60854251f25e7de94928686f647e6e0f07ca40188e8dfd8140985f1dfbb53bb4d92ff42c29e216431500839d7eb83b81f386bac3a8a86dd8c986a611ec0db3a4  kaffeine-2.0.18.tar.xz"
