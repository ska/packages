# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@alpinelinux.org>
# Maintainer: Sheila Aman <sheila@vulpine.house>
pkgname=wayland
pkgver=1.19.0
pkgrel=0
pkgdesc="A computer display server protocol"
url="https://wayland.freedesktop.org"
arch="all"
license="MIT"
depends=""
depends_dev="libffi-dev expat-dev"
makedepends="$depends_dev doxygen xmlto graphviz grep libxml2-dev bash"
subpackages="$pkgname-dev"
replaces="wayland-libs-client wayland-libs-cursor wayland-libs-server"
source="https://wayland.freedesktop.org/releases/$pkgname-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-documentation \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

dev() {
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$pkgdir"/usr/share \
		"$subpkgdir"/usr
	default_dev
}

sha512sums="d8a86f5e23e4a88e7c84b82fdb51eb350419086afe462ecb2f4d5c3ba9290ede310cbbcffd60215219ddccf5bad4adec21a5ebfbef6577200f66ac7a1b64a5ef  wayland-1.19.0.tar.xz"
