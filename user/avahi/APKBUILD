# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=avahi
pkgver=0.8
pkgrel=1
pkgdesc="Local network service discovery library"
url="https://avahi.org/"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="dbus-dev expat-dev glib-dev gobject-introspection-dev
	gtk+3.0-dev libdaemon-dev libevent-dev qt5-qtbase-dev doxygen
	py3-dbus-python py3-pygobject-dev"
pkgusers="avahi avahi-autoipd"
pkggroups="avahi avahi-autoipd"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-openrc
	$pkgname-gtk3 $pkgname-py3 $pkgname-qt5"
install="$pkgname.pre-install $pkgname.pre-upgrade"
source="https://avahi.org/download/avahi-$pkgver.tar.gz
	build-db
	"

prepare() {
	default_prepare
	# Missing from release tarballs:
	# https://github.com/lathiat/avahi/pull/281
	mv "$srcdir"/build-db "$builddir"/service-type-database/
}

build() {
	[ "$CBUILD_ARCH" = "$CTARGET_ARCH" ] || \
		die "You cannot cross-build Avahi; dbm files are not portable."
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-distro=gentoo \
		--enable-core-docs \
		--disable-mono \
		--disable-gdbm \
		--enable-compat-libdns_sd
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

gtk3() {
	pkgdesc="$pkgdesc (Gtk+ 3 bindings)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libavahi*gtk3* "$subpkgdir"/usr/lib/
}

py3() {
	pkgdesc="$pkgdesc (Python bindings)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/python* "$subpkgdir"/usr/lib/
}

qt5() {
	pkgdesc="$pkgdesc (Qt 5 bindings)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libavahi*qt5* "$subpkgdir"/usr/lib/
}

sha512sums="c6ba76feb6e92f70289f94b3bf12e5f5c66c11628ce0aeb3cadfb72c13a5d1a9bd56d71bdf3072627a76cd103b9b056d9131aa49ffe11fa334c24ab3b596c7de  avahi-0.8.tar.gz
60f5cab8417ba1f7ef9b3e30a9f33923fd71a11ae846dda9c29fd0327008821f55f6c77f8ef8442e6dd164d1b777d858cec95c31b7b3bc7f1121417620ca5f08  build-db"
