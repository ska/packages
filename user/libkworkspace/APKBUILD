# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=libkworkspace
pkgver=5.18.5
pkgrel=0
pkgdesc="KDE Plasma 5 workspace library"
url="https://www.kde.org/plasma-desktop"
arch="all"
options="!check"  # Test requires X11 accelration.
license="(GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1+ AND GPL-2.0+ AND MIT AND LGPL-2.1-only AND LGPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-only"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="cmake extra-cmake-modules libice-dev libsm-dev libxau-dev
	kcoreaddons-dev ki18n-dev kscreenlocker-dev kwin-dev kwindowsystem-dev
	plasma-framework-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-workspace-$pkgver.tar.xz
	session.patch
	standalone.patch
	"
builddir="$srcdir"/plasma-workspace-$pkgver/libkworkspace

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS} . \
		-Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="870cf89649d9498831f4ef9b21d3c07504b7fc7b09b95dd7e0a1d356b41fbfceed1c4f27aa258bcf1e23cfe915d31701c155325fcd4944f9cc957a287ebc1ee2  plasma-workspace-5.18.5.tar.xz
27d801b0d05c5fbcd172f4ff60d537f8ebc8d2658202ed290203effc7c9b81599d0bd1c644afada265200d2d48669a3762a66310780dcc23d0ea19fa7865f62b  session.patch
4e0559902e3c718260a1f8bfdc8110b6c4dcc0647f2060a991ca27ce2c1c1b070f3d58512c5ef37db1216924a014b0ae84e2d28b2e546b113551e3d75dd2652b  standalone.patch"
