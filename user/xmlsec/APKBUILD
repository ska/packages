# Maintainer: Max Rees <maxcrees@me.com>
pkgname=xmlsec
pkgver=1.2.30
_pkgname="$pkgname${pkgver%%.*}"
pkgrel=0
pkgdesc="C-based XML signature and encryption syntax and processing library"
url="https://www.aleksey.com/xmlsec/"
arch="all"
license="MIT"
depends=""
checkdepends="nss-tools"
makedepends="libtool libxml2-dev libxslt-dev openssl-dev
	gnutls-dev libgcrypt-dev nss-dev"
subpackages="$pkgname-dev $pkgname-doc
	$pkgname-gcrypt $pkgname-gnutls $pkgname-nss"
source="http://www.aleksey.com/xmlsec/download/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-static \
		--enable-pkgconfig \
		--with-openssl \
		--with-gnutls \
		--with-gcrypt \
		--with-default-crypto='openssl'
	make
}

check() {
	make -k check
}

package() {
	make DESTDIR="$pkgdir" install

	install -m755 -d "$pkgdir/usr/share/licenses/$pkgname"
	install -m644 'COPYING' "$pkgdir/usr/share/licenses/$pkgname/"
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/xmlsec1Conf.sh "$subpkgdir"/usr/lib
}

gcrypt() {
	pkgdesc="xmlsec gcrypt plugin"
	install_if="$pkgname=$pkgver-r$pkgrel gcrypt"
	mkdir -p "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libxmlsec1-gcrypt.so* "$subpkgdir"/usr/lib/
}

gnutls() {
	pkgdesc="xmlsec gnutls plugin"
	install_if="$pkgname=$pkgver-r$pkgrel gnutls"
	mkdir -p "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libxmlsec1-gnutls.so* "$subpkgdir"/usr/lib/
}

nss() {
	pkgdesc="xmlsec NSS plugin"
	install_if="$pkgname=$pkgver-r$pkgrel nss"
	mkdir -p "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libxmlsec1-nss.so* "$subpkgdir"/usr/lib/
}

sha512sums="07152470a9fe5d077f8a358608ca1d8a79ee0d2777660f61ed5717dc640714a3adfe66843e6a4023898eb0f5ed79771d70c41132571f3a1aeda82c1894b69c98  xmlsec1-1.2.30.tar.gz"
