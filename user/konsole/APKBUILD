# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=konsole
pkgver=20.08.1
pkgrel=0
pkgdesc="Terminal emulator for Qt/KDE"
url="https://konsole.kde.org/"
arch="all"
options="!check"  # Requires running DBus session bus.
license="GPL-2.0-only AND LGPL-2.1+ AND Unicode-DFS-2016"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kbookmarks-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kcrash-dev kguiaddons-dev kdbusaddons-dev ki18n-dev kiconthemes-dev
	kinit-dev kio-dev knotifications-dev knotifyconfig-dev kparts-dev
	kpty-dev kservice-dev ktextwidgets-dev kwidgetsaddons-dev python3
	kwindowsystem-dev kxmlgui-dev kdbusaddons-dev knewstuff-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/konsole-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="3321b97ee738eee10a7bc7f1085b1180a4cbbad67c587149a9acacd96439b89ec5e463028a44f9b9590308614d2c37acca7b31748e290274021031528ec7b995  konsole-20.08.1.tar.xz"
