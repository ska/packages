# Contributor: Sheila Aman <sheila@vulpine.house>
# Maintainer: Sheila Aman <sheila@vulpine.house>
pkgname=pcmanfm-qt
pkgver=0.17.0
_lxqt=0.9.0
pkgrel=0
pkgdesc="File manager and desktop icon manager for LXQt"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=$_lxqt
	liblxqt-dev>=${pkgver%.*}.0 libfm-qt-dev>=${pkgver%.*}.0
	qt5-qtx11extras-dev qt5-qttools-dev kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/pcmanfm-qt/releases/download/$pkgver/pcmanfm-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="6de9083b90a2d06db892941e50dc9817e1ab8cf5bab80e331cd083c4756b7ede1207ca35fc1fc43b81b52651acb57dfd85953ab8f8deef2a2c0d362374726daa  pcmanfm-qt-0.17.0.tar.xz"
