# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=elogind
pkgver=243.7
pkgrel=0
pkgdesc="Session manager system"
url=" "
arch="all"
license="LGPL-2.1+"
depends="dbus eudev utmps"
makedepends="meson ninja acl-dev dbus-dev docbook-xsl eudev-dev gperf libcap-dev
	libxslt-dev linux-headers linux-pam-dev m4 skalibs-libs-dev
	utmps-libs-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-openrc"
install="$pkgname.post-install $pkgname.post-deinstall"
source="elogind-$pkgver.tar.gz::https://github.com/elogind/elogind/archive/v$pkgver.tar.gz
	utmps.patch

	elogind.pamd
	elogind.confd
	elogind.initd
	"

build() {
	export LDFLAGS="-Wl,--no-as-needed -l:libutmps.so -Wl,--as-needed"
	# Note: We can't use openrc as our cgroup controller,
	# otherwise we aren't portable to s6/runit later.
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		-Dcgroup-controller=elogind \
		-Ddefault-kill-user-processes=false \
		-Dman=auto \
		build
	ninja -C build
}

check() {
	ninja -C build test
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
	install -D -m644 "$srcdir"/elogind.pamd "$pkgdir"/etc/pam.d/elogind-user
	install -D -m755 "$srcdir"/elogind.initd "$pkgdir"/etc/init.d/elogind
	install -D -m644 "$srcdir"/elogind.confd "$pkgdir"/etc/conf.d/elogind
}

sha512sums="fd17c5016d083d63805f9ed0326ee32597870f6c48c9c246712cb09a77db775036b0fd0f4258b6557e189a1eceb4b50f4ae2e9e7881f4d9759b87a0b49ce3472  elogind-243.7.tar.gz
5c1596b85e893e965ffb6f401c51d07dc4a77468b2ccfbe93a2b84eae596acf8a31a760c16f174cf73ffd6d404e9252e9baa7ea5ecc7785e6bf66282c27c66fc  utmps.patch
b5043d7bcbefdf00704d40670deafa0984899cc7be083e8d886a92137932a9f23c25749c106cfc07906166e57db32fe6c602cf1c54129faa7e5b04d6228b7c17  elogind.pamd
2c9047c054582824f8a3e71bb0a79a5621d7a92cb15c66bb7e7a8deb931037b6d3a331b86cdc9eb5ec4e7b2b725548a7ee6657e3c3193d81ba6422cd101a2491  elogind.confd
bbc203a924abf65b45d7d4d2fc31baf2d4e23135d98c7c937f93a4fe2d4ce9dac3d4e0747c1e09101f8b2ce674d25d49e085bf2445e7cb28408d7d209f8f4491  elogind.initd"
